import { createApp } from 'vue'
import App from './App.vue'
import 'vuetify/dist/vuetify.min.css'
import { VTable, VTextField } from 'vuetify/lib/components'
import { createVuetify } from 'vuetify'
import router from './router'

const app = createApp(App).use(router)

const vuetify = createVuetify({
  components: {
    VTable,
    VTextField,
  },
})

app.use(vuetify)

app.mount('#app')
