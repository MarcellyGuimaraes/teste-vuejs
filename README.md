# Teste Vuejs

## Setup inicial do projeto
```
npm install
```

### Rodar o servidor
```
npm run serve
```

### Rodar a api json-server
```
npm install -g json-server (caso não tenha o json-server na máquina)
json-server --watch db.json
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
